package com.company;

import com.company.models.Course;
import com.company.models.Lesson;
import com.company.models.Teacher;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LessonRepositoryJdbcTempImpl implements LessonRepository{

    //language=SQL
    private static final String SQL_SELECT_BY_NAME = "select * from lesson where lesson_name = ?";

    //language=SQL
    private static final String SQL_UPDATE = "update lesson set lesson_name = ?, time_day = ?, course = ? where id = ?";

    //language=SQL
    private static final String SQL_INSERT = "insert into lesson(lesson_name, time_day, course) values (?, ?, ?)";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select *, c.id as course_id , c.name as course_name " +
            "from lesson l left join course c on l.course = c.id where l.id = ?";

    private JdbcTemplate jdbcTemplate;

    public LessonRepositoryJdbcTempImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    private final RowMapper<Teacher> teacherRowMapper = (row, rowNumber) -> {
        Integer id = row.getInt("teacher_id");
        String firstName = row.getString("first_name");
        String lastname = row.getString("last_name");
        Integer experience = row.getInt("experience");
        return new Teacher(id, firstName, lastname, experience);
    };

    private final RowMapper<Course> courseRowMapper = (row, rowNumber) -> {
        int id = row.getInt("course_id");
        String course_name = row.getString("course_name");
        String timetable = row.getString("timetable");
        Teacher teacher = teacherRowMapper.mapRow(row, rowNumber);

        Course course = new Course(id, course_name, timetable, teacher);
        course.setStudents(new ArrayList<>());
        return course;
    };

    private final RowMapper<Lesson> lessonRowMapper = (row, rowNumber) -> {

        int id = row.getInt("lesson_id");
        String lesson_name = row.getString("lesson_name");
        String time_day = row.getString("time_day");
        Course course = courseRowMapper.mapRow(row, rowNumber);
        Lesson lesson = new Lesson(id, lesson_name, time_day, course);
        return lesson;
    };


    private final ResultSetExtractor<Lesson> lessonResultSetExtractor = resultSet -> {
        Lesson lesson = null;
        if (resultSet.next()) {
            int id = resultSet.getInt("lesson_id");
            String lesson_name = resultSet.getString("lesson_name");
            String time_day = resultSet.getString("time_day");
            lesson = new Lesson(id, lesson_name, time_day);
            do {
                Integer courseId = resultSet.getObject("course", Integer.class);
                if (courseId != null) {
                    int CourseId = resultSet.getInt("course_id");
                    String course_name = resultSet.getString("course_name");
                    String timetable = resultSet.getString("timetable");
                    Course course = new Course(CourseId, course_name, timetable);
                    lesson.setCourse(course);
                }
            } while (resultSet.next());
        }
        return lesson;
    };
    @Override
    public void save(Lesson lesson) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT, new String[] {"id"});
            statement.setString(1, lesson.getLesson_name());
            statement.setString(2, lesson.getTime_day());
            statement.setInt(3, lesson.getCourse().getId());

            return statement;
        }, keyHolder);
        lesson.setId(keyHolder.getKey().intValue());
    }

    @Override
    public void update(Lesson lesson) {
        jdbcTemplate.update(SQL_UPDATE, lesson.getLesson_name(), lesson.getTime_day()
                ,lesson.getCourse().getId(), lesson.getId());
    }

    @Override
    public Optional<Lesson> findById(Integer id) {
        try {
            return Optional.of(jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, lessonRowMapper, id));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<Lesson> findByName(String name) {
        return jdbcTemplate.query(SQL_SELECT_BY_NAME, lessonRowMapper, name);
    }
}
