package com.company;

import com.company.models.Course;
import com.company.models.Student;
import com.company.models.Teacher;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class CoursesRepositoryJdbcTempImpl implements CourseRepository{

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select *, " +
            "s.first_name as student_first_name, s.last_name as student_last_name from course as c " +
            "left join course_student cs on c.id = cs.course_id " +
            "left join student s on cs.student_id = s.id " +
            "where c.id = ? ";

    //language=SQL
    private static final String SQL_SELECT_BY_NAME = "select * from course where course_name = ?";

    //language=SQL
    private static final String SQL_UPDATE = "update course set course_name = ?, timetable = ?, teacher = ? where id = ?";

    //language=SQL
    private static final String SQL_INSERT = "insert into course(course_name, timetable, teacher) values (?, ?, ?)";

    private JdbcTemplate jdbcTemplate;

    public CoursesRepositoryJdbcTempImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private final RowMapper<Teacher> teacherRowMapper = (row, rowNumber) -> {
        Integer id = row.getInt("teacher_id");
        String firstName = row.getString("first_name");
        String lastname = row.getString("last_name");
        Integer experience = row.getInt("experience");
        return new Teacher(id, firstName, lastname, experience);
    };

    private final RowMapper<Course> courseRowMapper = (row, rowNumber) -> {
        int id = row.getInt("course_id");
            String course_name = row.getString("course_name");
            String timetable = row.getString("timetable");
            Teacher teacher = teacherRowMapper.mapRow(row, rowNumber);

            Course course = new Course(id, course_name, timetable, teacher);
        course.setStudents(new ArrayList<>());
        return course;
    };

    private final RowMapper<Student> studentRowMapper = (row, rowNumber) -> {
        int id = row.getInt("student_id");
        String first_name = row.getString("first_name");
        String last_name = row.getString("last_name");
        int group_number = row.getInt("group_number");
        return new Student(id, first_name, last_name, group_number);
    };


    private final ResultSetExtractor<Course> courseResultSetExtractor = resultSet -> {
        Course course = null;
        if (resultSet.next()) {
            course = courseRowMapper.mapRow(resultSet, resultSet.getRow());
            course.setStudents(new ArrayList<>());
            do {
                Integer id = resultSet.getObject("student_id", Integer.class);
                if (id != null) {
                    Student student = studentRowMapper.mapRow(resultSet, resultSet.getRow());
                    course.getStudents().add(student);
                }
            } while (resultSet.next());
        }
        return course;
    };

    @Override
    public void save(Course course) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
           PreparedStatement statement = connection.prepareStatement(SQL_INSERT, new String[] {"id"});
           statement.setString(1, course.getCourse_name());
           statement.setString(2, course.getTimetable());
           statement.setInt(3, course.getTeacher().getId());

           return statement;
                }, keyHolder);
        course.setId(keyHolder.getKey().intValue());
    }

    @Override
    public void update(Course course) {
        jdbcTemplate.update(SQL_UPDATE, course.getCourse_name(), course.getTimetable()
                ,course.getTeacher().getId(), course.getId());
    }

    @Override
    public Optional<Course> findById(Integer id) {
        try {
            return Optional.of(jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, courseRowMapper, id));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<Course> findByName(String name) {
        return jdbcTemplate.query(SQL_SELECT_BY_NAME, courseRowMapper, name);
    }
}
