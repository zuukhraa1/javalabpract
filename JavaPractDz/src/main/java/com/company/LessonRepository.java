package com.company;


import com.company.models.Lesson;

import java.util.List;
import java.util.Optional;

public interface LessonRepository {
    void save(Lesson lesson);
    void update(Lesson lesson);
    Optional<Lesson> findById(Integer id);
    List<Lesson> findByName(String name);
}
