package com.company.models;

import java.util.ArrayList;
import java.util.StringJoiner;

public class Student {
    private Integer id;
    private String first_name;
    private String last_name;
    private Integer group_number;
    private ArrayList<Course> courses;

    public Student(Integer id, String first_name, String last_name, Integer group_number) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.group_number = group_number;
    }

    public Student(Integer id, String first_name, String last_name, Integer group_number, ArrayList<Course> courses) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.group_number = group_number;
        this.courses = courses;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public Integer getGroup_number() {
        return group_number;
    }

    public void setGroup_number(Integer group_number) {
        this.group_number = group_number;
    }

    public ArrayList<Course> getCourses() {
        return courses;
    }

    public void setCourses(ArrayList<Course> courses) {
        this.courses = courses;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Student.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("first_name='" + first_name + "'")
                .add("last_name='" + last_name + "'")
                .add("group_number=" + group_number)
                .add("courses=" + courses)
                .toString();
    }
}
