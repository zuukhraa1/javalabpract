package com.company.models;

import java.util.ArrayList;
import java.util.StringJoiner;

public class Course {
    private Integer id;
    private String course_name;
    private String timetable;
    private Teacher teacher;
    private ArrayList<Student> students;

    public Course(Integer id, String course_name, String timetable) {
        this.id = id;
        this.course_name = course_name;
        this.timetable = timetable;
    }

    public Course(Integer id, String course_name, String timetable, Teacher teacher, ArrayList<Student> students) {
        this.id = id;
        this.course_name = course_name;
        this.timetable = timetable;
        this.teacher = teacher;
        this.students = students;
    }

    public Course(Integer id, String course_name, String timetable, Teacher teacher) {
        this.id = id;
        this.course_name = course_name;
        this.timetable = timetable;
        this.teacher = teacher;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getTimetable() {
        return timetable;
    }

    public void setTimetable(String timetable) {
        this.timetable = timetable;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Course.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("course_name='" + course_name + "'")
                .add("timetable='" + timetable + "'")
                .add("teacher=" + teacher)
                .add("students=" + students)
                .toString();
    }
}
