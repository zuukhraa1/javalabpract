package com.company.models;

import java.util.StringJoiner;

public class Lesson {
    private Integer id;
    private String lesson_name;
    private String time_day;
    private Course course;

    public Lesson(Integer id, String lesson_name, String time_day) {
        this.id = id;
        this.lesson_name = lesson_name;
        this.time_day = time_day;
    }

    public Lesson(Integer id, String lesson_name, String time_day, Course course) {
        this.id = id;
        this.lesson_name = lesson_name;
        this.time_day = time_day;
        this.course = course;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLesson_name() {
        return lesson_name;
    }

    public void setLesson_name(String lesson_name) {
        this.lesson_name = lesson_name;
    }

    public String getTime_day() {
        return time_day;
    }

    public void setTime_day(String time_day) {
        this.time_day = time_day;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Lesson.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("lesson_name='" + lesson_name + "'")
                .add("time_day='" + time_day + "'")
                .add("course=" + course.getCourse_name())
                .toString();
    }
}
