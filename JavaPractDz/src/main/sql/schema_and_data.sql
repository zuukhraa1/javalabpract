create table teacher (
                         id serial primary key,
                         first_name varchar(20),
                         last_name varchar(20),
                         experience integer
);

insert into teacher (first_name, last_name, experience)
values ('Михаил', 'Абрамский', '15');
insert into teacher (first_name, last_name, experience)
values ('Марат', 'Арсланов', '30');


create table course (
                        id serial primary key,
                        course_name varchar(20),
                        timetable varchar(20),
                        teacher_id integer,
                        foreign key (teacher_id) references teacher(id)
);

insert into course (course_name, timetable, teacher_id)
values ('Высшая математика', '08.09.21 - 25.05.22', 2);
insert into course (course_name, timetable, teacher_id)
values ('Программирование', '10.10.21 - 13.07.22', 1);


create table lesson (
                        id serial primary key,
                        lesson_name varchar(20),
                        time_day varchar(20),
                        course_id integer,
                        foreign key (course_id) references course(id)
);

insert into lesson (lesson_name, time_day, course_id)
values ('Геометрия', 'Понедельник, 10:10', 1);
insert into lesson (lesson_name, time_day, course_id)
values ('Алгебра', 'Пятница, 15:30', 1);
insert into lesson (lesson_name, time_day, course_id)
values ('Java', 'Вторник, 11:50', 2);
insert into lesson (lesson_name, time_day, course_id)
values ('Python', 'Среда, 11:50', 2);


create table student (
                         id serial primary key,
                         first_name varchar(20),
                         last_name varchar(20),
                         group_number integer
);

insert into student (first_name, last_name, group_number)
values ('Зухра', 'Шагиахметова', '4');
insert into student (first_name, last_name, group_number)
values ('Эльмира', 'Байгулова', '4');
insert into student (first_name, last_name, group_number)
values ('Рамиль', 'Гатин', '3');


create table course_student (
                                student_id integer,
                                course_id integer,
                                foreign key (student_id) references student(id),
                                foreign key (course_id) references course(id)
);

insert into course_student (student_id, course_id)
values (1, 1);
insert into course_student (student_id, course_id)
values (1, 2);
insert into course_student (student_id, course_id)
values (2, 1);
insert into course_student (student_id, course_id)
values (3, 2);




